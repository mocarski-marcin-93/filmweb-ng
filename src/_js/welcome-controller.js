var welcome = app.controller('welcome-controller', function($scope) {

    this.editMode = false;
    this.editStatusNotOk = false;
    this.editPrompt = '';
    var _this = this;

    this.changeUserName = function() {
        this.editMode = true;
        var editInput = document.getElementById('editUserName');
        editInput.addEventListener('mouseout', mouseoutEvent);
        // Mouseout = warn user that he is in edit mode
        function mouseoutEvent(event) {
            _this.editStatusNotOk = true;
            _this.editPrompt = "Wciśnij [enter] aby potwierdzić!";
            $scope.$apply();
            // Mouseover again = hide prompt
            document.getElementById('editUserName').addEventListener('mouseover', function(event) {
                _this.editStatusNotOk = false;
                $scope.$apply();
            });
        };
        // Enter = user wants to apply his name
        editInput.addEventListener('keypress', function(event) {
            var keyCode = event.keyCode;
            if (keyCode === 13) {
                if(event.target.value.length < 5 || event.target.value.length > 9) {
                  _this.editStatusNotOk = true;
                  _this.editPrompt = "Od 5 do 9 znakow!";
                  $scope.$apply();
                  return;
                }
                _this.editMode = false;
                _this.editStatusNotOk = false;
                $scope.$apply();
                editInput.removeEventListener('mouseout', mouseoutEvent);
            }
        });
    };
});
