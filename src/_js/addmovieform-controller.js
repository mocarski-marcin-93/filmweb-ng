var addmovieform = app.controller('addmovieform-controller', function($scope, $rootScope) {

    this.title = '';
    this.author = '';
    this.description = '';
    this.image = '';
    this.imageUrl = '';
    this.added = false;
    this.readyToAdd = false;
    this.uploadstatus = '';
    var _this = this;

    function showImage(event) {

        var file = event.target.files[0];
        _this.image = file.name;

        // http://www.html5rocks.com/en/tutorials/file/dndfiles/
        var reader = new FileReader();
        reader.onload = (function(theFile) {
            if (!file.type.match('image.*')) {
                _this.readyToAdd = false;
                return;
            }
            return function(event) {
                _this.imageUrl = event.target.result;

                _this.readyToAdd = true;
                $scope.$apply();
            };
        })(file);
        reader.readAsDataURL(file);
    };
    document.getElementById('files').addEventListener('change', showImage, false);

    this.submitForm = function() {
        if (!_this.readyToAdd) {
            _this.printStatus('failure');
            return;
        }
        $rootScope.user.movies.push({
            title: _this.title,
            img: _this.imageUrl
        });
        $rootScope.baseMovies.movies.push({
          title: _this.title,
          img: _this.imageUrl
        })
        _this.printStatus('success');
    };

    this.printStatus = function(status) {
        if (status === 'failure') {
            _this.uploadstatus = "Dodawanie nie powiodło się.\n Sprobuj ponownie.";
            document.getElementById('uploadstatusdiv').setAttribute('class', 'uploadstatus failure');
            document.getElementsByTagName('body')[0].setAttribute('class', 'inactive');
            document.getElementById('uploadstatusclose').addEventListener('click', function() {
                document.getElementById('uploadstatusdiv').setAttribute('class', 'uploadstatus');
                document.getElementsByTagName('body')[0].setAttribute('class', '');
            });
        }
        if (status === "success") {
            _this.uploadstatus = "Dodawanie powiodło się.\n Film " + _this.title + " dodano do bazy.";
            document.getElementById('uploadstatusdiv').setAttribute('class', 'uploadstatus success');
            document.getElementsByTagName('body')[0].setAttribute('class', 'inactive');
            document.getElementById('uploadstatusclose').addEventListener('click', function() {
                document.getElementById('uploadstatusdiv').setAttribute('class', 'uploadstatus');
                document.getElementsByTagName('body')[0].setAttribute('class', '');
            });
        }
    }

});
