var app = angular.module('filmweb', []);

var main = app.controller('main-controller', function($scope, $rootScope) {

    var _this = this;

    $rootScope.user = {
        name: 'guest',
        avatar: '_img/user.png',
        movies: []
    };

    $rootScope.baseMovies = {
      movies: [{
          title: 'Some movie',
          img: '_img/example2.jpg'
      }, {
          title: 'Harry Potter and the Philosophers Stone',
          img: '_img/example.jpg'
      }, {
          title: 'def',
          img: '_img/example5.jpg'
      }, {
          title: 'def',
          img: '_img/example4.jpg'
      }, {
          title: 'def',
          img: '_img/example2.jpg'
      }, {
          title: 'def',
          img: '_img/example.jpg'
      }, {
          title: 'def',
          img: '_img/example5.jpg'
      }]
    };
});
