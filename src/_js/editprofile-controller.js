var editprofile = app.controller('editprofile-controller', function($scope, $rootScope) {

    this.name = '';
    this.image = '';
    this.imageUrl = '';
    this.added = false;
    this.readyToAdd = false;
    this.uploadstatus = '';
    var _this = this;

    function showImage(event) {

        var file = event.target.files[0];
        _this.image = file.name;

        // http://www.html5rocks.com/en/tutorials/file/dndfiles/
        var reader = new FileReader();
        reader.onload = (function(theFile) {
            if (!file.type.match('image.*')) {
                _this.readyToAdd = false;
                return;
            }
            return function(event) {
                _this.imageUrl = event.target.result;

                _this.readyToAdd = true;
                $scope.$apply();
            };
        })(file);
        reader.readAsDataURL(file);
    };
    document.getElementById('filesEditProfile').addEventListener('change', showImage, false);

    this.submitForm = function() {
        if (!_this.readyToAdd) {
            _this.printStatus('failure');
            return;
        }
        $rootScope.user.name = _this.name;
        $rootScope.user.avatar = _this.imageUrl;
        _this.printStatus('success');
    };

    this.printStatus = function(status) {
        if (status === 'failure') {
            _this.uploadstatus = "Edycja profilu nie powiodła się.\n Sprobuj ponownie.";
            document.getElementById('uploadstatusdivEditProfile').setAttribute('class', 'uploadstatus failure');
            document.getElementsByTagName('body')[0].setAttribute('class', 'inactive');
            document.getElementById('uploadstatuscloseEditProfile').addEventListener('click', function() {
                document.getElementById('uploadstatusdivEditProfile').setAttribute('class', 'uploadstatus');
                document.getElementsByTagName('body')[0].setAttribute('class', '');
            });
        }
        if (status === "success") {
            _this.uploadstatus = "Edycja profilu powiodła się.";
            document.getElementById('uploadstatusdivEditProfile').setAttribute('class', 'uploadstatus success');
            document.getElementsByTagName('body')[0].setAttribute('class', 'inactive');
            document.getElementById('uploadstatuscloseEditProfile').addEventListener('click', function() {
                document.getElementById('uploadstatusdivEditProfile').setAttribute('class', 'uploadstatus');
                document.getElementsByTagName('body')[0].setAttribute('class', '');
            });
        }
    }

});
