var navigation = app.controller('navigation-controller', function($scope) {

    var _this = this;

    this.tabsOpened = {
        myMovies: false,
        addMovie: false,
        editProfile: false
    }

    this.openTab = function(which) {
        switch (which) {
            case 'myMovies':
                {
                    document.getElementById('mymovies').setAttribute('class', 'container');
                    _this.tabsOpened.myMovies = true;
                }
                break;

            case 'addMovie':
                {
                    document.getElementById('addmovie').setAttribute('class', 'container addmoviecontainer');
                    _this.tabsOpened.addMovie = true;
                }
                break;
            case 'editProfile':
                {
                    document.getElementById('editprofile').setAttribute('class', 'container editprofilecontainer');
                    _this.tabsOpened.editProfile = true;
                }
                break;
            case 'topTen':
                {
                    setTimeout(function() {
                        document.getElementById('mainSection').className = 'section1';
                        document.getElementById('topTenSection').className = 'section4 visible';
                    }, 0);
                }
                break;
            case 'baseMovies':
                {
                    setTimeout(function() {
                        document.getElementById('mainSection').className = 'section1';
                        document.getElementById('baseMoviesSection').className = 'section5 visible';
                    }, 0);
                }
                break;
            default:
                break;
        }
    }

    this.closeTab = function(which) {
        switch (which) {
            case 'myMovies':
                {
                    document.getElementById('mymovies').setAttribute('class', 'container close');
                    setTimeout(function() {
                        _this.tabsOpened.myMovies = false;
                        $scope.$apply();
                    }, 500);
                }
                break;
            case 'addMovie':
                {
                    document.getElementById('addmovie').setAttribute('class', 'container addmoviecontainer close');
                    setTimeout(function() {
                        _this.tabsOpened.addMovie = false;
                        $scope.$apply();
                    }, 500);
                }
                break;
            case 'editProfile':
                {
                    document.getElementById('editprofile').setAttribute('class', 'container editprofilecontainer close');
                    setTimeout(function() {
                        _this.tabsOpened.editProfile = false;
                        $scope.$apply();
                    }, 500);
                }
                break;
        }
    }
});
